//
//  ViewController.swift
//  AugmentedImages
//
//  Created by Maurizio Minieri on 22/12/2019.
//  Copyright © 2019 Maurizio Minieri. All rights reserved.
//

import UIKit
import ARKit
import SceneKit

class ViewController: UIViewController {
	
	/// Primary SceneKit view that renders the AR session
	@IBOutlet var sceneView: ARSCNView!
	
	/// A serial queue for thread safety when modifying SceneKit's scene graph.
	let updateQueue = DispatchQueue(label: "\(Bundle.main.bundleIdentifier!).serialSCNQueue")
	var objPlayer: AVAudioPlayer?
	
	
	// Called after the controller's view is loaded into memory.
	override func viewDidLoad() {
		super.viewDidLoad()
		
		// Set the view's delegate
		sceneView.delegate = self
		
		// Show statistics such as FPS and timing information (useful during development)
		sceneView.showsStatistics = true
		
		// Enable environment-based lighting
		sceneView.autoenablesDefaultLighting = true
		sceneView.automaticallyUpdatesLighting = true
	}
	
	// Notifies the view controller that its view is about to be added to a view hierarchy.
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		//refImages will be one of the images contained in the AR Resources folder
		guard let refImages = ARReferenceImage.referenceImages(inGroupNamed: "AR Resources", bundle: Bundle.main) else {
			fatalError("Missing expected asset catalog resources.")
		}
		
		// Create a session configuration
		let configuration = ARImageTrackingConfiguration()
		configuration.trackingImages = refImages
		configuration.maximumNumberOfTrackedImages = 1
		
		// Run the view's session
		sceneView.session.run(configuration, options: ARSession.RunOptions(arrayLiteral: [.resetTracking, .removeExistingAnchors]))
	}
	
	// Notifies the view controller that its view is about to be removed from a view hierarchy.
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		// Pause the view's session
		sceneView.session.pause()
	}
	
	//play audio
	func playAudioFile(name: String) {
		guard let url = Bundle.main.url(forResource: name, withExtension: "mp3") else { return }
		
		do {
			try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
			try AVAudioSession.sharedInstance().setActive(true)
			
			// For iOS 11
			objPlayer = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
			
			guard let aPlayer = objPlayer else { return }
			aPlayer.play()
			
		} catch let error {
			print(error.localizedDescription)
		}
	}
	
}


extension ViewController: ARSCNViewDelegate {
	
	// MARK: - ARSCNViewDelegate
	
	func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
		
		//NO LOOP
		// if the anchor is not of type ARImageAnchor (which means image is not detected), just return
		/*guard let imageAnchor = anchor as? ARImageAnchor else {return}
		//find our video file
		let videoNode = SKVideoNode(fileNamed: "black.mp4")
		videoNode.play()
		*/
		

		//LOOP
		guard let imageAnchor = anchor as? ARImageAnchor, let fileUrlString = Bundle.main.path(forResource: imageAnchor.referenceImage.name, ofType: "mp4") else {return}
		//find our video file
		
		let videoItem = AVPlayerItem(url: URL(fileURLWithPath: fileUrlString))
		
		let player = AVPlayer(playerItem: videoItem)
		//initialize video node with avplayer
		let videoNode = SKVideoNode(avPlayer: player)
		
		player.play()
		// add observer when our player.currentItem finishes player, then start playing from the beginning
		NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: player.currentItem, queue: nil) { (notification) in
			player.seek(to: CMTime.zero)
			player.play()
			//print("Looping Video")
		}
		
		// set the size (just a rough one will do)
		let videoScene = SKScene(size: CGSize(width: 1920, height: 1080))
		// center our video to the size of our video scene
		videoNode.position = CGPoint(x: videoScene.size.width / 2, y: videoScene.size.height / 2)
		// invert our video so it does not look upside down
		videoNode.yScale = -1.0
		// add the video to our scene
		videoScene.addChild(videoNode)
		// create a plan that has the same real world height and width as our detected image
		let plane = SCNPlane(width: imageAnchor.referenceImage.physicalSize.width, height: imageAnchor.referenceImage.physicalSize.height)
		// set the first materials content to be our video scene
		plane.firstMaterial?.diffuse.contents = videoScene
		// create a node out of the plane
		let planeNode = SCNNode(geometry: plane)
		// since the created node will be vertical, rotate it along the x axis to have it be horizontal or parallel to our detected image
		planeNode.eulerAngles.x = -Float.pi / 2
		// finally add the plane node (which contains the video node) to the added node
		node.addChildNode(planeNode)
	
		
		//updateQueue.async {
			let physicalWidth = imageAnchor.referenceImage.physicalSize.width
			let physicalHeight = imageAnchor.referenceImage.physicalSize.height
			let name = imageAnchor.referenceImage.name
			var sceneName: String
			var myUrl: String = ""
			
			switch name {
				case "foxCard":
					sceneName = "FoxScene"
					myUrl = "https://en.wikipedia.org/wiki/Fox"
				case "elephantCard":
					sceneName = "ElephantScene"
					myUrl = "https://en.wikipedia.org/wiki/Elephant"
				case "penguinCard":
					sceneName = "PenguinScene"
					myUrl = "https://en.wikipedia.org/wiki/Penguin"
				case "owlCard":
					sceneName = "OwlScene"
					myUrl = "https://en.wikipedia.org/wiki/Owl"
				default: sceneName = ""
			}
			
			
			// Create a plane geometry to visualize the initial position of the detected image
			let mainPlane = SCNPlane(width: physicalWidth, height: physicalHeight)
			mainPlane.firstMaterial?.colorBufferWriteMask = .alpha
			
			// Create a SceneKit root node with the plane geometry to attach to the scene graph
			// This node will hold the virtual UI in place
			let mainNode = SCNNode(geometry: mainPlane)
			mainNode.eulerAngles.x = -.pi / 2
			mainNode.renderingOrder = -1
			mainNode.opacity = 1
			
			// Add the plane visualization to the scene
			node.addChildNode(mainNode)
			
			// Perform a quick animation to visualize the plane on which the image was detected.
			// We want to let our users know that the app is responding to the tracked image.
			self.highlightDetection(name: sceneName, url: myUrl, on: mainNode, width: physicalWidth, height: physicalHeight, completionHandler: {
				
				//play audio
				self.playAudioFile(name:"doppiaemme_image_detected")
				
				// Introduce virtual content
				self.displayDetailView(name: sceneName, on: mainNode, xOffset: physicalWidth)
				
				// Animate the WebView to the right
				self.displayWebView(url: myUrl, on: mainNode, xOffset: physicalWidth)
				
			})
		//}
	}
	
	func session(_ session: ARSession, didFailWithError error: Error) {
		// Present an error message to the user
		
	}
	
	func sessionWasInterrupted(_ session: ARSession) {
		// Inform the user that the session has been interrupted, for example, by presenting an overlay
	}
	
	func sessionInterruptionEnded(_ session: ARSession) {
		// Reset tracking and/or remove existing anchors if consistent tracking is required
	}
	
	// MARK: - SceneKit Helpers
	
	func displayDetailView(name: String, on rootNode: SCNNode, xOffset: CGFloat) {
		//i create the plain that will contain DetailScene.sks
		let detailPlane = SCNPlane(width: xOffset, height: xOffset * 1.4)
		detailPlane.cornerRadius = 0.25
		
		let detailNode = SCNNode(geometry: detailPlane)
		detailNode.geometry?.firstMaterial?.diffuse.contents = SKScene(fileNamed: name)
		
		// Due to the origin of the iOS coordinate system, SCNMaterial's content appears upside down, so flip the y-axis.
		detailNode.geometry?.firstMaterial?.diffuse.contentsTransform = SCNMatrix4Translate(SCNMatrix4MakeScale(1, -1, 1), 0, 1, 0)
		detailNode.position.z -= 0.5   //allineo sull'asse z
		detailNode.opacity = 0
		
		rootNode.addChildNode(detailNode)
		detailNode.runAction(.sequence([
			.wait(duration: 0.0),
			.fadeOpacity(to: 1.0, duration: 0.0),
			.moveBy(x: xOffset * -1.1, y: 0, z: -0.05, duration: 1.0), //verso sinistra
			.moveBy(x: 0, y: 0, z: -0.05, duration: 0.2)
		])
		)
	}
	
	func displayWebView(url: String, on rootNode: SCNNode, xOffset: CGFloat) {
		// Xcode yells at us about the deprecation of UIWebView in iOS 12.0, but there is currently
		// a bug that does now allow us to use a WKWebView as a texture for our webViewNode
		// Note that UIWebViews should only be instantiated on the main thread!
		DispatchQueue.main.async {
			let request = URLRequest(url: URL(string: url)!)
			let webView = UIWebView(frame: CGRect(x: 0, y: 0, width: 400, height: 672))
			webView.loadRequest(request)
			
			let webViewPlane = SCNPlane(width: xOffset, height: xOffset * 1.4)
			webViewPlane.cornerRadius = 0.25
			
			let webViewNode = SCNNode(geometry: webViewPlane)
			webViewNode.geometry?.firstMaterial?.diffuse.contents = webView
			webViewNode.position.z -= 0.5
			webViewNode.opacity = 0
			
			rootNode.addChildNode(webViewNode)
			webViewNode.runAction(.sequence([
				//.wait(duration: 0.4),
				.fadeOpacity(to: 1.0, duration: 0.5),
				.moveBy(x: xOffset * 1.1, y: 0, z: -0.05, duration: 1.0), //verso destra
				.moveBy(x: 0, y: 0, z: -0.05, duration: 0.2)
			])
			)
		}
	}
	
	func highlightDetection(name:String, url:String, on rootNode: SCNNode, width: CGFloat, height: CGFloat, completionHandler block: @escaping (() -> Void)) {
		let planeNode = SCNNode(geometry: SCNPlane(width: width, height: height))
		planeNode.geometry?.firstMaterial?.diffuse.contents = UIColor.white
		planeNode.position.z += 0.1
		planeNode.opacity = 0
		
		rootNode.addChildNode(planeNode)
		planeNode.runAction(self.imageHighlightAction) {
			block()
		}
	}
	
	var imageHighlightAction: SCNAction {
		return .sequence([
			//.wait(duration: 0.25),
			.fadeOpacity(to: 0.85, duration: 0.25),
			.fadeOpacity(to: 0.15, duration: 0.25),
			.fadeOpacity(to: 0.85, duration: 0.25),
			.fadeOut(duration: 0.5),
			.removeFromParentNode()
		])
	}
	
}
